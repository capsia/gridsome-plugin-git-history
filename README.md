# gridsome-plugin-git-history

A Gridsome plugin to add data about Git changes to data collections.

## Install

```sh
npm i --save gridsome-plugin-git-history
```

## Setup

```js
    {
      use: "gridsome-plugin-git-history",
      options: {
        typeName: "CollectionName",
        targetPath: "gitData",
        gitlog: {
          fields: ["subject", "authorName", "authorDate"]
        }
      }
    },
```

The typeName property is the name of the collection you want to
get the data. The targetPath property is the name of the property
where you want to store your git history data.

This package uses gitlog to parse git data. You'll need to make sure
that you have git installed on every machine that needs to build your
website. If you need more options for gitlog you can find them 
[here](https://www.npmjs.com/package/gitlog).
