const path = require('path')
const gitlog = require("gitlog").default;

class GitHistory {
    constructor(api, options) {

        this.options = options;
        this.api = api;

        let gitlogOptions = options.gitlog || {};

        gitlogOptions.repo = process.cwd();

        //add the git info field to all the elements in the collection to the targetPath
        api.onCreateNode((collectionElement) => {
            // Check if the collection is the correct one
            if (collectionElement.internal.typeName === options.typeName) {

                // Get relative file path for the current file
                let relativeFilePath = path.join(
                    collectionElement.fileInfo.directory,
                    collectionElement.fileInfo.name + collectionElement.fileInfo.extension
                );

                gitlogOptions.file = relativeFilePath;

                // Get gitlog using the gitlog npm package
                let commits = gitlog(gitlogOptions);

                // Format git dates to JS dates for GraphQL
                commits = commits.map(el => {
                  ["authorDate", "committerDate"].forEach(field => {
                    if (el[field]) {
                      el[field] = new Date(
                        el[field].replace(
                          /\+(\w{2})(\w{2})/,
                          "+$1:$2"
                        )
                      )
                    }
                  });
                  return el;
                });

                collectionElement[options.targetPath] = commits;
            }
        });
    }
}

module.exports = GitHistory
